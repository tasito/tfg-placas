#    Lector de placas
#    Copyright (C) 2019  - Ruben Hoffer

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


# Cuenta la cantidad de colonias asociadas a un tag de Custom Vision
# Configurar modelos, tagBuscada, probabilidadBase, headers y url

import os
import requests
import json
import time

probabilidadBase = 0.70 # indice de probabilidad a partir del cual se consideran validas las lecturas, de 0 a 1 con valores decimales
tagBuscada = "E coli" # tagName buscado para recuento (E coli - Placa)

# completar diccionario con enlaces de la API CustomVision (Prediction):
modelos = {
    'iter1' : 'https://eastus.api.cognitive.microsoft.com/customvision/v3.0/Prediction/.../detect/iterations/.../image',
    'iter2' : 'https://eastus.api.cognitive.microsoft.com/customvision/v3.0/Prediction/.../detect/iterations/.../image',
    'iter3' : 'https://eastus.api.cognitive.microsoft.com/customvision/v3.0/Prediction/.../detect/iterations/.../image',
    'iter4' : 'https://eastus.api.cognitive.microsoft.com/customvision/v3.0/Prediction/.../detect/iterations/.../image', 
    'iter5' : 'https://eastus.api.cognitive.microsoft.com/customvision/v3.0/Prediction/.../detect/iterations/.../image'
}

#completar prediction key de la API de Custom Vision:
headers = {'content-type': 'application/octet-stream', 'Prediction-Key' : 'ffffffffffffffffffffffffffffffff'} 

#relacionar con 
url = modelos['iter5']

path = '.\\images' #ruta donde se encuentran las imagenes a procesar

reporte = time.strftime("%Y-%m-%d_%H-%M-%S") #nombre del reporte en txt


fReporte = open(reporte+".txt", "a")
fReporte.write("Recuento en placa de imagenes\n")
fReporte.write("-----------------------------\n")
fReporte.write("Probabilidad base: " + str(probabilidadBase) + "\n")
fReporte.write("Tag buscada: " + tagBuscada + "\n")
fReporte.write("-----------------------------\n")
fReporte.write("API: + " + url +"\n")


files = []
for r, d, f in os.walk(path):
    for file in f:
        if '.jpeg' in file:
            files.append(os.path.join(r, file))

for f in files:    
    print(f)
    fReporte.write(f + "\n")
    image_file_descriptor = open(f, 'rb')    
    data=image_file_descriptor.read()
    r = requests.post(url, data=data, headers=headers)
    image_file_descriptor.close()
    tag = ""
    rto = 0
    for prediction in r.json()["predictions"]:
        tag = prediction["tagName"]
        if prediction["probability"]>=probabilidadBase and tag==tagBuscada:
            rto = rto + 1
    print(tagBuscada + " - Recuento en placa: " + str(rto))
    fReporte.write(tagBuscada + " - Recuento en placa: " + str(rto) + "\n")

fReporte.close()