# Lector de placas

Lector de imágenes de placas de Petri. Cuenta la cantidad de objetos asociados a un tag de Custom Vision.
Devuelve un reporte en txt con el recuento detectado por encima de la probabilidad base definida.

## Antes de empezar

Incluir imágenes a analizar en directorio /images o adaptar el path en el código.
Configurar modelos, tagBuscada, probabilidadBase, headers y url.

## Licencia

Lector de placas
Copyright (C) 2019  - Ruben Hoffer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Agradecimientos

* Damián Eiff